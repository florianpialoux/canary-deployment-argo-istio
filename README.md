# canary-deployment-argo-istio

This repository is used to test and demo a canary deployment using [Istio](https://istio.io/) and [Argo Rollouts](https://argoproj.github.io/rollouts/).


## Information about the application

Bookinfo application has been reused from Istio example and contains 4x seperate microservices:

- `productpage`. The `productpage` microservice calls the details and reviews microservices to populate the page.
- `details`. The `details` microservice contains book information.
- `reviews`. The `reviews` microservice contains book reviews. It also calls the `ratings` microservice.
- `ratings`. The `ratings` microservice contains book ranking information that accompanies a book review.

There are 3 versions of the reviews microservice:

- Version v1 doesn’t call the `ratings` service.
- Version v2 calls the `ratings` service, and displays each rating as 1 to 5 **black** stars.
- Version v3 calls the `ratings` service, and displays each rating as 1 to 5 **red** stars.

The end-to-end architecture of the application is shown below.

![bookinfo](./docs/bookinfo_diagram.svg)

ArgoCD Rollout manages traffic splitting with Istio by:
- modifying the VirtualService `spec.http[].route[].weight` to match the current desired canary weight
- modifying the DestinationRule `spec.subsets[].version` to contain the rollouts-pod-template-hash label of the canary and stable ReplicaSets

## How to provision this infrastructure

1. Run Terraform
````
$ cd terraform
$ terraform init
$ terraform apply
````
2. Fetch ArgoCD admin password
````
$ kubectl get secret argocd-initial-admin-secret -n argocd -o jsonpath="{.data.password}" | base64 -d
````
3. Login to ArgoCD
````
$ argocd login localhost:8080
````
4. Add this GitLab repository to ArgoCD
````
$ argocd repo add https://github.com/florianpialoux/canary-deployment-argo-istio.git --username '' --password 'glpat-xxxx'
````
5. Create bookinfo application on ArgoCD
````
$ argocd app create bookinfo \
    --dest-namespace default \
    --dest-server https://kubernetes.default.svc \
    --repo https://gitlab.com/florianpialoux/canary-deployment-argo-istio.git.git \
    --path application \
    --sync-policy automated
````
6. Create bookinfo rollout, configured as when a new version rollsout, 20% of the pods gets the new version, then waits for manual action – this is the pause: {} command. After someone approves it, it is automatically delivered to 40%, then 60%, then 80%, and finally 100% of the pods, with intervals of 10 seconds.
````
$ argocd app create reviews-rollout \
    --dest-namespace default \
    --dest-server https://kubernetes.default.svc \
    --repo https://gitlab.com/florianpialoux/canary-deployment-argo-istio.git.git \
    --path argocd/rollout \
    --sync-policy automated
````
7. Check ArgoCD Rollout dashboard, view status of the reviews-rollout -> http://localhost:3100/rollouts/default
````
$ kubectl port-forward svc/argo-rollouts-dashboard -n argocd 3100:3100
````
![argocd_rollout_paused](./docs/argocd_rollout_paused.png)
You can also run this CLI command to get similar results:
````
$ kubectl argo rollouts get rollout reviews-rollout
Name:            reviews-rollout
Namespace:       default
Status:          ✔ Healthy
Strategy:        Canary
  Step:          8/8
  SetWeight:     100
  ActualWeight:  100
Images:          docker.io/istio/examples-bookinfo-reviews-v1:1.18.0 (stable)
Replicas:
  Desired:       1
  Current:       1
  Updated:       1
  Ready:         1
  Available:     1

NAME                                         KIND        STATUS     AGE  INFO
⟳ reviews-rollout                            Rollout     ✔ Healthy  62s  
└──# revision:1                                                          
   └──⧉ reviews-rollout-867c6f496f           ReplicaSet  ✔ Healthy  62s  stable
      └──□ reviews-rollout-867c6f496f-b2bb5  Pod         ✔ Running  62s  ready:2/2
````
8. Update [spec.image](https://gitlab.com/florianpialoux/canary-deployment-argo-istio.git/-/blob/main/argocd/rollout/rollout.yaml?ref_type=heads#L22) ArgoCD will syncup this new change and update the deployment.
9. Again, you may use ArgoCD Rollout dashboard or query its status via the CLI. This time you would see that the rollout is paused until it's approved by someone.
````
$ kubectl argo rollouts get rollout reviews-rollout
Name:            reviews-rollout
Namespace:       default
Status:          ॥ Paused
Message:         CanaryPauseStep
Strategy:        Canary
  Step:          1/8
  SetWeight:     20
  ActualWeight:  20
Images:          docker.io/istio/examples-bookinfo-reviews-v1:1.18.0 (stable)
                 docker.io/istio/examples-bookinfo-reviews-v2:1.18.0 (canary)
Replicas:
  Desired:       1
  Current:       2
  Updated:       1
  Ready:         2
  Available:     2

NAME                                         KIND        STATUS     AGE    INFO
⟳ reviews-rollout                            Rollout     ॥ Paused   9m30s  
├──# revision:2                                                            
│  └──⧉ reviews-rollout-5bdff44cb6           ReplicaSet  ✔ Healthy  5s     canary
│     └──□ reviews-rollout-5bdff44cb6-hdj8p  Pod         ✔ Running  5s     ready:2/2
└──# revision:1                                                            
   └──⧉ reviews-rollout-867c6f496f           ReplicaSet  ✔ Healthy  9m30s  stable
      └──□ reviews-rollout-867c6f496f-b2bb5  Pod         ✔ Running  9m30s  ready:2/2
````
10. Approve the promotion and see its status once it upgraded to v2.
![argocd_rollout_v2_promoted](./docs/argocd_rollout_v2_promoted.png)
````
$ kubectl argo rollouts promote reviews-rollout
rollout 'reviews-rollout' promoted
$ kubectl argo rollouts get rollout reviews-rollout
Name:            reviews-rollout
Namespace:       default
Status:          ✔ Healthy
Strategy:        Canary
  Step:          8/8
  SetWeight:     100
  ActualWeight:  100
Images:          docker.io/istio/examples-bookinfo-reviews-v2:1.18.0 (stable)
Replicas:
  Desired:       1
  Current:       1
  Updated:       1
  Ready:         1
  Available:     1

NAME                                         KIND        STATUS        AGE    INFO
⟳ reviews-rollout                            Rollout     ✔ Healthy     17m    
├──# revision:2                                                               
│  └──⧉ reviews-rollout-5bdff44cb6           ReplicaSet  ✔ Healthy     8m32s  stable
│     └──□ reviews-rollout-5bdff44cb6-hdj8p  Pod         ✔ Running     8m32s  ready:2/2
└──# revision:1                                                               
   └──⧉ reviews-rollout-867c6f496f           ReplicaSet  • ScaledDown  17
````

## To do

- [ ] Check AnalysisRun

## References
- https://jimmysong.io/en/blog/implementing-gitops-and-canary-deployment-with-argo-project-and-istio
- https://istio.io/latest/docs/examples/bookinfo
- https://istio.io/latest/docs/concepts/traffic-management