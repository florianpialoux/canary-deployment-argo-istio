locals {
  ### https://github.com/argoproj/argo-helm/tree/main/charts/argo-rollouts
  argo_rollouts_helm_values = <<-EOF
    dashboard:
      enabled: true
  EOF
}

resource "helm_release" "argo_rollouts" {
  name       = "argo-rollouts"
  namespace  = "argocd"
  create_namespace = true
  repository = "https://argoproj.github.io/argo-helm"
  chart      = "argo-rollouts"
  version    = "2.32.4"
  values     = [local.argo_rollouts_helm_values]
}
