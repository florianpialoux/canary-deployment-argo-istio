locals {
  ### https://github.com/istio/istio/tree/master/manifests/charts/istio-control/istio-discovery
  istiod_base_helm_values = <<-EOF
  EOF
}

resource "helm_release" "istiod" {
  name             = "istiod"
  namespace        = "istio-system"
  repository       = "https://istio-release.storage.googleapis.com/charts"
  chart            = "istiod"
  create_namespace = true
  version          = "1.20.0"
  values           = [local.istiod_base_helm_values]
}
