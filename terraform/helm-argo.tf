locals {
  ### https://github.com/argoproj/argo-helm/tree/main/charts/argo-cd
  argocd_helm_values = <<-EOF
  EOF
}

resource "helm_release" "argocd" {
  name             = "argocd"
  namespace        = "argocd"
  create_namespace = true
  repository       = "https://argoproj.github.io/argo-helm"
  chart            = "argo-cd"
  version          = "5.51.4"
  values           = [local.argocd_helm_values]
}