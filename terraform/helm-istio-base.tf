locals {
  ### https://github.com/istio/istio/tree/master/manifests/charts/base
  istio_base_helm_values = <<-EOF
  EOF
}

resource "helm_release" "istio_base" {
  name             = "istio-base"
  namespace        = "istio-system"
  repository       = "https://istio-release.storage.googleapis.com/charts"
  chart            = "base"
  create_namespace = true
  version          = "1.20.0"
  values           = [local.istio_base_helm_values]
}

resource "kubernetes_labels" "istio_injection" {
  api_version = "v1"
  kind        = "Namespace"
  metadata {
    name = "default"
  }
  labels = {
    "istio-injection" = "enabled"
  }
}
