locals {
  ### https://github.com/istio/istio/tree/master/manifests/charts/gateway
  istio_gateway_helm_values = <<-EOF
    service:
      type: LoadBalancer
      ports:
      - name: tcp-port
        port: 15021
        protocol: TCP
        targetPort: 15021
      - name: http2-port
        port: 80
        protocol: TCP
        targetPort: 80
      - name: https-port
        port: 443
        protocol: TCP
        targetPort: 443
  EOF
}

resource "helm_release" "istio_ingressgateway" {
  name             = "istio-ingressgateway"
  namespace        = "istio-ingress"
  repository       = "https://istio-release.storage.googleapis.com/charts"
  chart            = "gateway"
  create_namespace = true
  version          = "1.20.0"
  values           = [local.istio_gateway_helm_values]
}
